package ro.deimios.ddelegatii.bean;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.primefaces.event.RowEditEvent;
import ro.deimios.ddelegatii.entity.Persoana;
import ro.deimios.ddelegatii.facade.PersoanaFacadeLocal;


/**
 *
 * @author deimios
 */
@ManagedBean
@RequestScoped
public class PersoaneEditBean {
    @EJB
    private PersoanaFacadeLocal persoanaFacade;

    /**
     * Creates a new instance of PersoaneEditBean
     */
    public PersoaneEditBean() {
    }
    
    public void onEdit(RowEditEvent event) {
        Persoana persoana = (Persoana) event.getObject();
        persoanaFacade.edit(persoana);
    }
}
