package ro.deimios.ddelegatii.bean;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import ro.deimios.ddelegatii.facade.LocatiiFacadeLocal;

/**
 *
 * @author deimios
 */
@ManagedBean
@RequestScoped
public class LocatiiAutocompleteBean {

    @EJB
    private LocatiiFacadeLocal locatiiFacade;

    /**
     * Creates a new instance of LocatiiAutocompleteBean
     */
    public LocatiiAutocompleteBean() {
    }
    
    //public List<Locatii> complete()
}
