package ro.deimios.ddelegatii.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import ro.deimios.ddelegatii.entity.Grupa;
import ro.deimios.ddelegatii.facade.GrupaFacadeLocal;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class GrupaListerBean implements Serializable {

    private List<Grupa> grupaList;
    @EJB
    private GrupaFacadeLocal grupaFacade;

    /**
     * Creates a new instance of GrupaListerBean
     */
    public GrupaListerBean() {
    }

    @PostConstruct
    public void loadData() {
        grupaList = grupaFacade.findAll();
    }

    public List<Grupa> getGrupaList() {
        return grupaList;
    }

    public void setGrupaList(List<Grupa> grupaList) {
        this.grupaList = grupaList;
    }
}
