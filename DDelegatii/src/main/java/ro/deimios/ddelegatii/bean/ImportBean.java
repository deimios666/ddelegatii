package ro.deimios.ddelegatii.bean;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.primefaces.event.FileUploadEvent;
import ro.deimios.ddelegatii.entity.Functia;
import ro.deimios.ddelegatii.entity.Grupa;
import ro.deimios.ddelegatii.entity.Locatii;
import ro.deimios.ddelegatii.entity.Persoana;
import ro.deimios.ddelegatii.entity.Scop;
import ro.deimios.ddelegatii.facade.FunctiaFacadeLocal;
import ro.deimios.ddelegatii.facade.GrupaFacadeLocal;
import ro.deimios.ddelegatii.facade.LocatiiFacadeLocal;
import ro.deimios.ddelegatii.facade.PersoanaFacadeLocal;
import ro.deimios.ddelegatii.facade.ScopFacadeLocal;
import ro.deimios.ddelegatii.helper.BeanHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class ImportBean implements Serializable {

    @EJB
    private ScopFacadeLocal scopFacade;
    @EJB
    private LocatiiFacadeLocal locatiiFacade;
    @EJB
    private FunctiaFacadeLocal functiaFacade;
    @EJB
    private GrupaFacadeLocal grupaFacade;
    @EJB
    private PersoanaFacadeLocal persoanaFacade;

    /**
     * Creates a new instance of ImportBean
     */
    public ImportBean() {
    }

    public void handlePersoanaUpload(FileUploadEvent event) {
        System.out.println("Starting upload");
        InputStream fis = null;
        try {
            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
            if (userBean == null) {
                return;
            }
            fis = event.getFile().getInputstream();
            Workbook workbook = WorkbookFactory.create(fis);
            Sheet sheet = workbook.getSheetAt(0);
            //persoanaFacade.removeAll();
            //loop over rows
            Iterator<Row> rowWalker = sheet.rowIterator();
            Row ignoredHeaderRow = rowWalker.next();
            int readData = 0;
            while (rowWalker.hasNext()) {
                Row row = rowWalker.next();
                try {
                    String nume = row.getCell(0).getStringCellValue();
                    String functia = row.getCell(1).getStringCellValue();
                    String grupa;
                    String subgrupa;
                    DecimalFormat myFormat = new DecimalFormat("#");
                    try {
                        grupa = row.getCell(2).getStringCellValue();
                    } catch (Exception e) {
                        grupa = myFormat.format(row.getCell(2).getNumericCellValue());
                    }
                    try {
                        subgrupa = row.getCell(3).getStringCellValue();
                    } catch (Exception e) {
                        subgrupa = myFormat.format(row.getCell(3).getNumericCellValue());
                    }
                    String buletin = "";
                    try {
                        buletin = row.getCell(4).getStringCellValue();
                    } catch (Exception e) {
                    }

                    Functia dbFunctia = functiaFacade.findByNume(functia);
                    if (dbFunctia == null) {
                        dbFunctia = new Functia(0);
                        dbFunctia.setNume(functia);
                        functiaFacade.create(dbFunctia);
                    }

                    Grupa dbGrupa = grupaFacade.findByGrupaSubgrupa(grupa, subgrupa);
                    if (dbGrupa == null) {
                        dbGrupa = new Grupa(0);
                        dbGrupa.setGrupa(grupa);
                        dbGrupa.setSubgrupa(subgrupa);
                        grupaFacade.create(dbGrupa);
                    }

                    boolean isCreated = false;
                    Persoana persoana = persoanaFacade.findByNume(nume);
                    if (persoana == null) {
                        isCreated = true;
                        persoana = new Persoana(0);
                    }
                    persoana.setNume(nume);
                    persoana.setFunctiaId(dbFunctia);
                    persoana.setGrupaId(dbGrupa);
                    persoana.setBuletin(buletin);
                    if (isCreated) {
                        persoanaFacade.create(persoana);
                    } else {
                        persoanaFacade.edit(persoana);
                    }
                    readData++;

                } catch (Exception parseEx) {
                    Logger.getLogger(ImportBean.class.getName()).log(Level.SEVERE, null, parseEx);
                    break;
                }
            }

            fis.close();
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Importat cu Succes", readData + " persoane au fost importate cu succes!");

            FacesContext.getCurrentInstance()
                    .addMessage(null, msg);

        } catch (InvalidFormatException ex) {
            Logger.getLogger(ImportBean.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ImportBean.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();


            } catch (IOException ex) {
                Logger.getLogger(ImportBean.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }


    }

    public void handleDestinatiiUpload(FileUploadEvent event) {

        System.out.println("Starting upload");
        InputStream fis = null;
        try {
            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
            if (userBean == null) {
                return;
            }
            fis = event.getFile().getInputstream();
            Workbook workbook = WorkbookFactory.create(fis);
            Sheet sheet = workbook.getSheetAt(0);

            //loop over rows
            Iterator<Row> rowWalker = sheet.rowIterator();
            Row ignoredHeaderRow = rowWalker.next();
            int readData = 0;
            while (rowWalker.hasNext()) {
                Row row = rowWalker.next();
                try {
                    String nume = row.getCell(0).getStringCellValue();
                    Locatii dbLocatii = locatiiFacade.findByNume(nume);
                    if (dbLocatii == null) {
                        dbLocatii = new Locatii(0);
                        dbLocatii.setNume(nume);
                        locatiiFacade.create(dbLocatii);
                        readData++;


                    }

                } catch (Exception parseEx) {
                    Logger.getLogger(ImportBean.class
                            .getName()).log(Level.SEVERE, null, parseEx);

                    break;
                }
            }

            fis.close();
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Importat cu Succes", readData + " locații au fost importate cu succes!");
            FacesContext.getCurrentInstance().addMessage(null, msg);



        } catch (InvalidFormatException ex) {
            Logger.getLogger(ImportBean.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ImportBean.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();


            } catch (IOException ex) {
                Logger.getLogger(ImportBean.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void handleScopUpload(FileUploadEvent event) {

        System.out.println("Starting upload");
        InputStream fis = null;
        try {
            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
            if (userBean == null) {
                return;
            }
            fis = event.getFile().getInputstream();
            Workbook workbook = WorkbookFactory.create(fis);
            Sheet sheet = workbook.getSheetAt(0);

            //loop over rows
            Iterator<Row> rowWalker = sheet.rowIterator();
            Row ignoredHeaderRow = rowWalker.next();
            int readData = 0;
            while (rowWalker.hasNext()) {
                Row row = rowWalker.next();
                try {
                    String line1 = row.getCell(0).getStringCellValue();
                    String line2 = row.getCell(1).getStringCellValue();
                    String line3 = row.getCell(2).getStringCellValue();
                    String line4 = row.getCell(3).getStringCellValue();
                    Scop dbScop = scopFacade.findByLines(line1, line2, line3, line4);
                    if (dbScop == null) {
                        dbScop = new Scop(0);
                        dbScop.setLine1(line1);
                        dbScop.setLine2(line2);
                        dbScop.setLine3(line3);
                        dbScop.setLine4(line4);
                        scopFacade.create(dbScop);
                        readData++;


                    }

                } catch (Exception parseEx) {
                    Logger.getLogger(ImportBean.class
                            .getName()).log(Level.SEVERE, null, parseEx);

                    break;
                }
            }
            fis.close();
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Importat cu Succes", readData + " scopuri au fost importate cu succes!");
            FacesContext.getCurrentInstance().addMessage(null, msg);



        } catch (InvalidFormatException ex) {
            Logger.getLogger(ImportBean.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ImportBean.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();


            } catch (IOException ex) {
                Logger.getLogger(ImportBean.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
