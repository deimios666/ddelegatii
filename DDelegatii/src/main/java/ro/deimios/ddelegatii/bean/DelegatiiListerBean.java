package ro.deimios.ddelegatii.bean;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import ro.deimios.ddelegatii.entity.Delegatii;
import ro.deimios.ddelegatii.facade.DelegatiiFacadeLocal;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class DelegatiiListerBean implements Serializable{

    @EJB
    private DelegatiiFacadeLocal delegatiiFacade;
    private List<Delegatii> delegatiiList;

    /**
     * Creates a new instance of DelegatiiListerBean
     */
    public DelegatiiListerBean() {
    }

    @PostConstruct
    public void loadData() {
        delegatiiList = delegatiiFacade.findAll();
        Collections.reverse(delegatiiList);
    }

    
    
    public List<Delegatii> getDelegatiiList() {
        return delegatiiList;
    }

    public void setDelegatiiList(List<Delegatii> delegatiiList) {
        this.delegatiiList = delegatiiList;
    }
}
