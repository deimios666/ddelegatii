package ro.deimios.ddelegatii.bean;

import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import ro.deimios.ddelegatii.entity.Persoana;
import ro.deimios.ddelegatii.facade.PersoanaFacadeLocal;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class PersoaneListerBean implements Serializable {

    private List<Persoana> persoaneList;
    private List<Persoana> filteredPersoaneList;
    @EJB
    private PersoanaFacadeLocal persoanaFacade;

    /**
     * Creates a new instance of PersoaneListerBean
     */
    public PersoaneListerBean() {
    }

    @PostConstruct
    public void loadData() {
        persoaneList = persoanaFacade.findAll();
    }

    public List<Persoana> getPersoaneList() {
        return persoaneList;
    }

    public void setPersoaneList(List<Persoana> persoaneList) {
        this.persoaneList = persoaneList;
    }

    public List<Persoana> getFilteredPersoaneList() {
        return filteredPersoaneList;
    }

    public void setFilteredPersoaneList(List<Persoana> filteredPersoaneList) {
        this.filteredPersoaneList = filteredPersoaneList;
    }
}
