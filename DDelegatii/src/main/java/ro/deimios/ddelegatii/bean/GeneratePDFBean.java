package ro.deimios.ddelegatii.bean;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import ro.deimios.ddelegatii.facade.DelegatiiFacadeLocal;
import ro.deimios.ddelegatii.facade.SettingsFacadeLocal;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class GeneratePDFBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(GeneratePDFBean.class.getName());
    @Resource(name = "DDELEGATII")
    private DataSource DDELEGATII;
    @EJB
    private SettingsFacadeLocal settingsFacade;
    @EJB
    private DelegatiiFacadeLocal delegatiiFacade;
    int minNr;
    int maxNr;
    private String versoType = "0";

    /**
     * Creates a new instance of GeneratePDFBean
     */
    public GeneratePDFBean() {
    }

    @PostConstruct
    public void loadData() {
        minNr = 0;
        maxNr = delegatiiFacade.getMaxNr();
    }

    public void downloadPDF() {
        try {
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext ec = fc.getExternalContext();
            ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
            ec.setResponseContentType("application/pdf");
            ec.setResponseHeader("Content-Disposition", "attachment; filename=\"delegatie_" + minNr + "_" + maxNr + ".pdf\"");
            InputStream inputStream = ec.getResourceAsStream("/templates/delegatii_rep.jasper");
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(inputStream);
            Map<String, Object> params = new HashMap<>();

            params.put("header", settingsFacade.getSetting("header"));
            params.put("minNr", minNr);
            params.put("maxNr", maxNr);
            JasperPrint jasperPrint;
            try (Connection conn = DDELEGATII.getConnection()) {
                jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            }
            File tmpA5 = File.createTempFile("macheta", ".pdf");
            String filename = tmpA5.getAbsolutePath() + File.pathSeparator + tmpA5.getName();
            JasperExportManager.exportReportToPdfFile(jasperPrint, filename);
            PdfReader reader = new PdfReader(filename);
            int pages = reader.getNumberOfPages();
            int halfPoint = (int) Math.ceil(pages / 2.0);

            InputStream versoInputStream;
            PdfReader versoReader;
            versoInputStream = ec.getResourceAsStream("/templates/delegatie_verso.pdf");
            versoReader = new PdfReader(versoInputStream);

            OutputStream outputStream = ec.getResponseOutputStream();
            //FileOutputStream outputStream = new FileOutputStream("report2.pdf");
            Document document = new Document(PageSize.A4.rotate());
            //Document document = new Document(PageSize.A5);
            PdfWriter writer = PdfWriter.getInstance(document, outputStream);
            document.open();
            PdfContentByte cb = writer.getDirectContent(); // Holds the PDF data
            PdfImportedPage page, versoPage;
            versoPage = writer.getImportedPage(versoReader, 1);


            for (int currentPage = 1; currentPage <= halfPoint; currentPage++) {

                if ("1".equals(versoType)) {
                    //if verso before page then insert here
                    //document.setPageSize(PageSize.A4);
                    document.newPage();
                    cb.addTemplate(versoPage, 0, -1, 1, 0, 0, PageSize.A4.getWidth());
                }

                //document.setPageSize(PageSize.A4.rotate());
                document.newPage();
                page = writer.getImportedPage(reader, currentPage);
                cb.addTemplate(page, 5, 0);
                //document.newPage();
                if ((currentPage + halfPoint) <= pages) {
                    page = writer.getImportedPage(reader, currentPage + halfPoint);
                    cb.addTemplate(page, 415, 0);
                    //cb.addTemplate(page, 0, 0);
                } else {
                }

                if ("2".equals(versoType)) {
                    //if verso after page then insert here
                    //document.setPageSize(PageSize.A4);
                    document.newPage();
                    cb.addTemplate(versoPage, 0, -1, 1, 0, 0, PageSize.A4.getWidth());
                }

            }
            outputStream.flush();
            document.close();
            reader.close();
            versoReader.close();
            inputStream.close();
            versoInputStream.close();
            writer.close();
            outputStream.close();
            fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.
        } catch (DocumentException | SQLException | JRException | IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    public int getMinNr() {
        return minNr;
    }

    public void setMinNr(int minNr) {
        this.minNr = minNr;
    }

    public int getMaxNr() {
        return maxNr;
    }

    public void setMaxNr(int maxNr) {
        this.maxNr = maxNr;
    }

    public String getVersoType() {
        return versoType;
    }

    public void setVersoType(String versoType) {
        this.versoType = versoType;
    }
}
