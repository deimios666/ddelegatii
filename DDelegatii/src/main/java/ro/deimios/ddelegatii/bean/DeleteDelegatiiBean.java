/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.bean;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import ro.deimios.ddelegatii.facade.DelegatiiFacadeLocal;
import ro.deimios.ddelegatii.helper.BeanHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@RequestScoped
public class DeleteDelegatiiBean {

    @EJB
    private DelegatiiFacadeLocal delegatiiFacade;
    private int minNr;
    private int maxNr;

    /**
     * Creates a new instance of DeleteDelegatiiBean
     */
    public DeleteDelegatiiBean() {
    }

    public void delete() {
        delegatiiFacade.deleteByMinMaxNr(minNr, maxNr);
        ((DelegatiiListerBean) BeanHelper.getBean("delegatiiListerBean")).loadData();
    }

    public int getMinNr() {
        return minNr;
    }

    public void setMinNr(int minNr) {
        this.minNr = minNr;
    }

    public int getMaxNr() {
        return maxNr;
    }

    public void setMaxNr(int maxNr) {
        this.maxNr = maxNr;
    }
}
