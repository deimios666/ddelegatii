package ro.deimios.ddelegatii.bean;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import ro.deimios.ddelegatii.entity.User;
import ro.deimios.ddelegatii.facade.UserFacadeLocal;
import ro.deimios.ddelegatii.helper.CryptHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@SessionScoped
public class UserBean implements Serializable {

    @EJB
    private UserFacadeLocal userFacade;
    private User currentUser;
    private String userName;
    private String password;
    private boolean loggedIn = false;

    /**
     * Creates a new instance of UserBean
     */
    public UserBean() {
    }

    public void doLogin() {
        User dbUser = userFacade.findByUsername(userName);
        if (dbUser != null) {
            currentUser = dbUser;
            loggedIn = true;
        }
        userName = null;
        password = null;
    }

    public void doLogout() {
        loggedIn = false;
        currentUser = null;
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        request.getSession(true).invalidate(); 
   }

    public void keepAlive() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpSession session = (HttpSession) externalContext.getSession(true);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = CryptHelper.sha512(password);
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }
}
