package ro.deimios.ddelegatii.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.RowEditEvent;
import ro.deimios.ddelegatii.entity.Settings;
import ro.deimios.ddelegatii.facade.SettingsFacadeLocal;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class SettingsBean implements Serializable {

    @EJB
    private SettingsFacadeLocal settingsFacade;
    private List<Settings> settingsList;

    /**
     * Creates a new instance of SettingsBean
     */
    public SettingsBean() {
    }

    @PostConstruct
    public void loadData() {
        settingsList = settingsFacade.findAll();
    }

    public void onEdit(RowEditEvent event) {
        Settings setting = (Settings) event.getObject();
        settingsFacade.edit(setting);
    }

    public List<Settings> getSettingsList() {
        return settingsList;
    }

    public void setSettingsList(List<Settings> settingsList) {
        this.settingsList = settingsList;
    }
}
