package ro.deimios.ddelegatii.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import ro.deimios.ddelegatii.datatype.GrupaSelection;
import ro.deimios.ddelegatii.datatype.GrupaType;
import ro.deimios.ddelegatii.datatype.MainGrupaSelection;
import ro.deimios.ddelegatii.entity.Delegatii;
import ro.deimios.ddelegatii.entity.Locatii;
import ro.deimios.ddelegatii.entity.Persoana;
import ro.deimios.ddelegatii.entity.Scop;
import ro.deimios.ddelegatii.facade.DelegatiiFacadeLocal;
import ro.deimios.ddelegatii.facade.GrupaFacadeLocal;
import ro.deimios.ddelegatii.facade.LocatiiFacadeLocal;
import ro.deimios.ddelegatii.facade.PersoanaFacadeLocal;
import ro.deimios.ddelegatii.facade.ScopFacadeLocal;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class GeneratorBean implements Serializable {

    @EJB
    private DelegatiiFacadeLocal delegatiiFacade;
    @EJB
    private PersoanaFacadeLocal persoanaFacade;
    @EJB
    private ScopFacadeLocal scopFacade;
    @EJB
    private LocatiiFacadeLocal locatiiFacade;
    @EJB
    private GrupaFacadeLocal grupaFacade;
    String nr;
    Date data;
    List<String> grupaList;
    List<GrupaType> grupaSubrgupaList;
    List<MainGrupaSelection> grupeTableBacker;
    List<GrupaSelection> subgrupeTableBacker;
    List<GrupaSelection> grupaSelection;
    List<Locatii> locatiiList;
    List<Scop> scopList;
    Integer generationProgress = 0;

    @PostConstruct
    public void loadData() {
        grupaList = grupaFacade.findGrupe();
        grupaSubrgupaList = grupaFacade.findGrupeSubgrupe();
        subgrupeTableBacker = new ArrayList<GrupaSelection>();
        for (GrupaType grupa : grupaSubrgupaList) {
            subgrupeTableBacker.add(new GrupaSelection(grupa));
        }
        grupaSelection = new ArrayList<GrupaSelection>();

        grupeTableBacker = new ArrayList<MainGrupaSelection>();
        for (String grupa : grupaList) {
            grupeTableBacker.add(new MainGrupaSelection(grupa, subgrupeTableBacker));
        }
        locatiiList = locatiiFacade.findAll();
        scopList = scopFacade.findAll();
        if (nr == null) {
            nr = "" + (delegatiiFacade.getMaxNr() + 1);
        }
    }

    /**
     * Creates a new instance of GeneratorBean
     */
    public GeneratorBean() {
    }

    public void doSaveSelection() {
        for (GrupaSelection grupaSubGrupa : subgrupeTableBacker) {
            if (grupaSubGrupa.getLocatie() != null) {
                grupaSelection.add(grupaSubGrupa);
            }
        }
    }

    public void doClearSelection() {
        grupaSelection.clear();
    }

    public void doGenerateDelegatii() {
        int currentNr = Integer.valueOf(nr);
        int nrOfDelegatii = 0;
        int nrOfGrupe = 0;
        generationProgress = 0;
        for (GrupaSelection grupa : grupaSelection) {
            Locatii locatie = locatiiFacade.find(grupa.getLocatie().getId());
            Scop scop = scopFacade.find(grupa.getScop().getId());
            //select persoane with grupa
            List<Persoana> persoanaList = persoanaFacade.findByGrupaSubgrupa(grupa.getGrupa().getGrupa(), grupa.getGrupa().getSubgrupa());
            //create delegatii for each persoana
            for (Persoana persoana : persoanaList) {
                Delegatii delegatie = new Delegatii(0, currentNr, data, grupa.getDeLa());
                delegatie.setEndDate(grupa.getPanaLa());
                delegatie.setLocatiiId(locatie);
                delegatie.setPersoanaId(persoana);
                delegatie.setScopId(scop);
                delegatiiFacade.create(delegatie);
                nrOfDelegatii++;
                currentNr++;
            }
            nrOfGrupe++;
            generationProgress = Math.round((nrOfGrupe / grupaSelection.size() * 100));
        }
    }

    public String getNr() {
        return nr;
    }

    public void setNr(String nr) {
        this.nr = nr;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public List<String> getGrupaList() {
        return grupaList;
    }

    public List<GrupaType> getGrupaSubrgupaList() {
        return grupaSubrgupaList;
    }

    public List<GrupaSelection> getGrupaSelection() {
        return grupaSelection;
    }

    public void setGrupaSelection(List<GrupaSelection> grupaSelection) {
        this.grupaSelection = grupaSelection;
    }

    public List<GrupaSelection> getSubgrupeTableBacker() {
        return subgrupeTableBacker;
    }

    public void setSubgrupeTableBacker(List<GrupaSelection> subgrupeTableBacker) {
        this.subgrupeTableBacker = subgrupeTableBacker;
    }

    public List<Locatii> getLocatiiList() {
        return locatiiList;
    }

    public List<Scop> getScopList() {
        return scopList;
    }

    public List<MainGrupaSelection> getGrupeTableBacker() {
        return grupeTableBacker;
    }

    public void setGrupeTableBacker(List<MainGrupaSelection> grupeTableBacker) {
        this.grupeTableBacker = grupeTableBacker;
    }

    public Integer getGenerationProgress() {
        return generationProgress;
    }

    public void setGenerationProgress(Integer generationProgress) {
        this.generationProgress = generationProgress;
    }
}
