package ro.deimios.ddelegatii.bean;

import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import ro.deimios.ddelegatii.entity.Persoana;
import ro.deimios.ddelegatii.facade.PersoanaFacadeLocal;

/**
 *
 * @author deimios
 */
@ManagedBean
@RequestScoped
public class PersoanaAutocompleteBean {

    @EJB
    private PersoanaFacadeLocal persoanaFacade;

    /**
     * Creates a new instance of PersoanaAutocompleteBean
     */
    public PersoanaAutocompleteBean() {
    }

    public List<Persoana> complete(String query) {
        return persoanaFacade.findByNumeLikeUpper(query);
    }
}
