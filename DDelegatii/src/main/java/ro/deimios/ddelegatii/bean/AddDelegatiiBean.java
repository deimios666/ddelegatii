package ro.deimios.ddelegatii.bean;

import java.io.Serializable;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import ro.deimios.ddelegatii.entity.Locatii;
import ro.deimios.ddelegatii.entity.Persoana;
import ro.deimios.ddelegatii.entity.Scop;
import ro.deimios.ddelegatii.facade.DelegatiiFacadeLocal;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class AddDelegatiiBean implements Serializable {

    @EJB
    private DelegatiiFacadeLocal delegatiiFacade;
    int nr;
    Date data;
    Scop scop;
    Persoana persoana;
    Locatii locatie;
    Date deLa;
    Date panaLa;

    /**
     * Creates a new instance of AddDelegatiiBean
     */
    public AddDelegatiiBean() {
    }

    @PostConstruct
    public void loadData() {
        nr = delegatiiFacade.getMaxNr() + 1;
        data = new Date();
        
        deLa = new Date();
        panaLa = new Date();
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Scop getScop() {
        return scop;
    }

    public void setScop(Scop scop) {
        this.scop = scop;
    }

    public Persoana getPersoana() {
        return persoana;
    }

    public void setPersoana(Persoana persoana) {
        this.persoana = persoana;
    }

    public Locatii getLocatie() {
        return locatie;
    }

    public void setLocatie(Locatii locatie) {
        this.locatie = locatie;
    }

    public Date getDeLa() {
        return deLa;
    }

    public void setDeLa(Date deLa) {
        this.deLa = deLa;
    }

    public Date getPanaLa() {
        return panaLa;
    }

    public void setPanaLa(Date panaLa) {
        this.panaLa = panaLa;
    }
}

