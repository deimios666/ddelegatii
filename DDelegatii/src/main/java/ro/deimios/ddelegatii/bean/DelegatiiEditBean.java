package ro.deimios.ddelegatii.bean;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.primefaces.event.RowEditEvent;
import ro.deimios.ddelegatii.entity.Delegatii;
import ro.deimios.ddelegatii.facade.DelegatiiFacadeLocal;

/**
 *
 * @author deimios
 */
@ManagedBean
@RequestScoped
public class DelegatiiEditBean {

    @EJB
    private DelegatiiFacadeLocal delegatiiFacade;

    /**
     * Creates a new instance of DelegatiiEditBean
     */
    public DelegatiiEditBean() {
    }

    public void onEdit(RowEditEvent event) {
        Delegatii delegatii = (Delegatii) event.getObject();
        delegatiiFacade.edit(delegatii);
    }
}
