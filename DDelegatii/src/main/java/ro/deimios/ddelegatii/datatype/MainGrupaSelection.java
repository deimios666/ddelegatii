package ro.deimios.ddelegatii.datatype;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import ro.deimios.ddelegatii.entity.Locatii;
import ro.deimios.ddelegatii.entity.Scop;

/**
 *
 * @author deimios
 */
public class MainGrupaSelection {

    private String grupa;
    private List<GrupaSelection> grupaList;
    private Locatii locatie;
    private Scop scop;
    private Date deLa;
    private Date panaLa;

    public MainGrupaSelection(String grupa, List<GrupaSelection> grupaList) {
        this.grupa = grupa;

        this.grupaList = new ArrayList<GrupaSelection>();

        for (GrupaSelection grupaSelection : grupaList) {
            if (grupaSelection.getGrupa().getGrupa().equals(this.grupa)) {
                this.grupaList.add(grupaSelection);
            }
        }

    }

    public String getGrupa() {
        return grupa;
    }

    public List<GrupaSelection> getGrupaList() {
        return grupaList;
    }

    public Locatii getLocatie() {
        return locatie;
    }

    public void setLocatie(Locatii locatie) {
        for (int i = 0; i < grupaList.size(); i++) {
            grupaList.get(i).setLocatie(locatie);
        }
        this.locatie = locatie;
    }

    public Scop getScop() {
        return scop;
    }

    public void setScop(Scop scop) {
        for (int i = 0; i < grupaList.size(); i++) {
            grupaList.get(i).setScop(scop);
        }
        this.scop = scop;
    }

    public Date getDeLa() {
        return deLa;
    }

    public void setDeLa(Date deLa) {
        for (int i = 0; i < grupaList.size(); i++) {
            grupaList.get(i).setDeLa(deLa);
        }
        this.deLa = deLa;
    }

    public Date getPanaLa() {
        return panaLa;
    }

    public void setPanaLa(Date panaLa) {
        for (int i = 0; i < grupaList.size(); i++) {
            grupaList.get(i).setPanaLa(panaLa);
        }
        this.panaLa = panaLa;
    }
}
