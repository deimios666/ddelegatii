package ro.deimios.ddelegatii.datatype;

/**
 *
 * @author deimios
 */
public class GrupaType {

    private String grupa;
    private String subgrupa;

    public GrupaType(String grupa, String subgrupa) {
        this.grupa = grupa;
        this.subgrupa = subgrupa;
    }

    public String getGrupa() {
        return grupa;
    }

    public void setGrupa(String grupa) {
        this.grupa = grupa;
    }

    public String getSubgrupa() {
        return subgrupa;
    }

    public void setSubgrupa(String subgrupa) {
        this.subgrupa = subgrupa;
    }
}
