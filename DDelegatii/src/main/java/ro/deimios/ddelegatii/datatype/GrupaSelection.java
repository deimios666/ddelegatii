package ro.deimios.ddelegatii.datatype;

import java.util.Date;
import ro.deimios.ddelegatii.entity.Locatii;
import ro.deimios.ddelegatii.entity.Scop;

/**
 *
 * @author deimios
 */
public class GrupaSelection {

    private GrupaType grupa;
    private Locatii locatie;
    private Scop scop;
    private Date deLa;
    private Date panaLa;

    public GrupaSelection() {
    }

    public GrupaSelection(GrupaType grupa) {
        this.grupa = grupa;
    }

    public GrupaSelection(GrupaType grupa, Locatii locatie, Scop scop, Date deLa, Date panaLa) {
        this.grupa = grupa;
        this.locatie = locatie;
        this.scop = scop;
        this.deLa = deLa;
        this.panaLa = panaLa;
    }

    public GrupaType getGrupa() {
        return grupa;
    }

    public void setGrupa(GrupaType grupa) {
        this.grupa = grupa;
    }

    public Locatii getLocatie() {
        return locatie;
    }

    public void setLocatie(Locatii locatie) {
        this.locatie = locatie;
    }

    public Scop getScop() {
        return scop;
    }

    public void setScop(Scop scop) {
        this.scop = scop;
    }

    public Date getDeLa() {
        return deLa;
    }

    public void setDeLa(Date deLa) {
        this.deLa = deLa;
    }

    public Date getPanaLa() {
        return panaLa;
    }

    public void setPanaLa(Date panaLa) {
        this.panaLa = panaLa;
    }
}
