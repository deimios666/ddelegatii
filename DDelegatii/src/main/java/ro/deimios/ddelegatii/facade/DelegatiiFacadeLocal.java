/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.ddelegatii.entity.Delegatii;

/**
 *
 * @author deimios
 */
@Local
public interface DelegatiiFacadeLocal {

    void create(Delegatii delegatii);

    void edit(Delegatii delegatii);

    void remove(Delegatii delegatii);

    Delegatii find(Object id);

    List<Delegatii> findAll();

    List<Delegatii> findRange(int[] range);

    int count();

    public int getMaxNr();

    public void deleteByMinMaxNr(int minNr, int maxNr);
    
}
