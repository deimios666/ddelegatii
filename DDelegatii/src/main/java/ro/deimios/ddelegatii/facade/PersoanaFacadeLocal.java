/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.ddelegatii.entity.Persoana;

/**
 *
 * @author deimios
 */
@Local
public interface PersoanaFacadeLocal {

    void create(Persoana persoana);

    void edit(Persoana persoana);

    void remove(Persoana persoana);

    Persoana find(Object id);

    List<Persoana> findAll();

    List<Persoana> findRange(int[] range);

    int count();

    public void removeAll();

    public java.util.List<ro.deimios.ddelegatii.entity.Persoana> findByGrupaSubgrupa(java.lang.String grupa, java.lang.String subgrupa);

    public java.util.List<ro.deimios.ddelegatii.entity.Persoana> findByNumeLikeUpper(java.lang.String nume);

    public ro.deimios.ddelegatii.entity.Persoana findByNume(java.lang.String nume);
    
}
