/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.ddelegatii.entity.Functia;

/**
 *
 * @author deimios
 */
@Stateless
public class FunctiaFacade extends AbstractFacade<Functia> implements FunctiaFacadeLocal {

    @PersistenceContext(unitName = "DDelegatiiPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FunctiaFacade() {
        super(Functia.class);
    }

    @Override
    public Functia findByNume(String nume) {
        Query query = em.createNamedQuery("Functia.findByNume");
        query.setParameter("nume", nume);
        List<Functia> result = query.getResultList();
        try {
            Functia functia = result.get(0);
            return functia;
        } catch (Exception ex) {
            return null;
        }

    }
}
