/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.ddelegatii.entity.Scop;

/**
 *
 * @author deimios
 */
@Stateless
public class ScopFacade extends AbstractFacade<Scop> implements ScopFacadeLocal {

    @PersistenceContext(unitName = "DDelegatiiPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ScopFacade() {
        super(Scop.class);
    }

    public Scop findByLines(String line1, String line2, String line3, String line4) {
        Query query = em.createNamedQuery("Scop.findByLines");
        query.setParameter("line1", line1);
        query.setParameter("line2", line2);
        query.setParameter("line3", line3);
        query.setParameter("line4", line4);
        List<Scop> result = query.getResultList();
        try {
            Scop dbScop = result.get(0);
            return dbScop;
        } catch (Exception ex) {
            return null;
        }

    }
}
