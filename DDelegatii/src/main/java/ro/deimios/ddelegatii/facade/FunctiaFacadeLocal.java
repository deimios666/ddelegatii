/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.ddelegatii.entity.Functia;

/**
 *
 * @author deimios
 */
@Local
public interface FunctiaFacadeLocal {

    void create(Functia functia);

    void edit(Functia functia);

    void remove(Functia functia);

    Functia find(Object id);

    List<Functia> findAll();

    List<Functia> findRange(int[] range);

    int count();

    public ro.deimios.ddelegatii.entity.Functia findByNume(java.lang.String nume);
    
}
