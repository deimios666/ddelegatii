/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.ddelegatii.entity.Persoana;

/**
 *
 * @author deimios
 */
@Stateless
public class PersoanaFacade extends AbstractFacade<Persoana> implements PersoanaFacadeLocal {

    @PersistenceContext(unitName = "DDelegatiiPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PersoanaFacade() {
        super(Persoana.class);
    }

    @Override
    public void removeAll() {
        Query query = em.createNamedQuery("Persoana.removeAll");
        query.executeUpdate();
    }

    @Override
    public List<Persoana> findByGrupaSubgrupa(String grupa, String subgrupa) {
        Query query = em.createNamedQuery("Persoana.findByGrupaSubgrupa");
        query.setParameter("grupa", grupa);
        query.setParameter("subgrupa", subgrupa);
        return query.getResultList();
    }

    @Override
    public Persoana findByNume(String nume) {
        Query query = em.createNamedQuery("Persoana.findByNume");
        query.setParameter("nume", nume);
        List<Persoana> result = query.getResultList();
        try {
            Persoana dbPersoana = result.get(0);
            return dbPersoana;
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public List<Persoana> findByNumeLikeUpper(String nume) {
        Query query = em.createNamedQuery("Persoana.findByNumeLikeUpper");
        query.setParameter("nume", "%" + nume.toUpperCase() + "%");
        return query.getResultList();
    }
}
