/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.ddelegatii.entity.Locatii;

/**
 *
 * @author deimios
 */
@Local
public interface LocatiiFacadeLocal {

    void create(Locatii locatii);

    void edit(Locatii locatii);

    void remove(Locatii locatii);

    Locatii find(Object id);

    List<Locatii> findAll();

    List<Locatii> findRange(int[] range);

    int count();

    public ro.deimios.ddelegatii.entity.Locatii findByNume(java.lang.String nume);
    
}
