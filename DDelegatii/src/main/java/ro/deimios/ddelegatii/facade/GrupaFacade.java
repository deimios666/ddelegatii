/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.facade;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.ddelegatii.datatype.GrupaType;
import ro.deimios.ddelegatii.entity.Grupa;

/**
 *
 * @author deimios
 */
@Stateless
public class GrupaFacade extends AbstractFacade<Grupa> implements GrupaFacadeLocal {

    @PersistenceContext(unitName = "DDelegatiiPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public GrupaFacade() {
        super(Grupa.class);
    }

    @Override
    public Grupa findByGrupaSubgrupa(String grupa, String subgrupa) {
        Query query = em.createNamedQuery("Grupa.findByGrupaSubgrupa");
        query.setParameter("grupa", grupa);
        query.setParameter("subgrupa", subgrupa);
        List<Grupa> result = query.getResultList();
        try {
            Grupa dbgrupa = result.get(0);
            return dbgrupa;
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public List<String> findGrupe() {
        Query query = em.createQuery("select g.grupa from Grupa g group by g.grupa");
        List<String> result = query.getResultList();
        return result;
    }

    @Override
    public List<GrupaType> findGrupeSubgrupe() {
        Query query = em.createQuery("select g.grupa, g.subgrupa from Grupa g group by g.grupa, g.subgrupa");
        List<GrupaType> returnList = new ArrayList();
        List<Object[]> result = query.getResultList();
        for (Object[] oneRow : result) {
            returnList.add(new GrupaType(oneRow[0].toString(), oneRow[1].toString()));
        }
        return returnList;
    }
}
