/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.facade;

import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.ddelegatii.entity.Delegatii;

/**
 *
 * @author deimios
 */
@Stateless
public class DelegatiiFacade extends AbstractFacade<Delegatii> implements DelegatiiFacadeLocal {

    private static final Logger LOG = Logger.getLogger(DelegatiiFacade.class.getName());
    @PersistenceContext(unitName = "DDelegatiiPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DelegatiiFacade() {
        super(Delegatii.class);
    }

    @Override
    public int getMaxNr() {
        Query query = em.createNamedQuery("Delegatii.getMaxNr");
        int maxNr = 0;
        //LOG.info((String) query.getSingleResult());
        try {
            //maxNr = (Integer) query.getSingleResult();
            if (!query.getResultList().isEmpty()) {
                maxNr = (Integer) query.getSingleResult();
            }
        } catch (Exception ex) {
        }
        return maxNr;
    }

    @Override
    public void deleteByMinMaxNr(int minNr, int maxNr) {
        Query query = em.createNamedQuery("Delegatii.deleteByMinMaxNr");
        query.setParameter("minNr", minNr);
        query.setParameter("maxNr", maxNr);
        query.executeUpdate();
    }
}
