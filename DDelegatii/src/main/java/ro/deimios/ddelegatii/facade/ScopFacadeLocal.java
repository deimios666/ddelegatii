/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.ddelegatii.entity.Scop;

/**
 *
 * @author deimios
 */
@Local
public interface ScopFacadeLocal {

    void create(Scop scop);

    void edit(Scop scop);

    void remove(Scop scop);

    Scop find(Object id);

    List<Scop> findAll();

    List<Scop> findRange(int[] range);

    int count();

    public ro.deimios.ddelegatii.entity.Scop findByLines(java.lang.String line1, java.lang.String line2, java.lang.String line3, java.lang.String line4);
    
}
