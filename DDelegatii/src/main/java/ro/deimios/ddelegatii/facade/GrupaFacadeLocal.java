/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.ddelegatii.entity.Grupa;

/**
 *
 * @author deimios
 */
@Local
public interface GrupaFacadeLocal {

    void create(Grupa grupa);

    void edit(Grupa grupa);

    void remove(Grupa grupa);

    Grupa find(Object id);

    List<Grupa> findAll();

    List<Grupa> findRange(int[] range);

    int count();

    public ro.deimios.ddelegatii.entity.Grupa findByGrupaSubgrupa(java.lang.String grupa, java.lang.String subgrupa);

    public java.util.List<java.lang.String> findGrupe();

    public java.util.List<ro.deimios.ddelegatii.datatype.GrupaType> findGrupeSubgrupe();
    
}
