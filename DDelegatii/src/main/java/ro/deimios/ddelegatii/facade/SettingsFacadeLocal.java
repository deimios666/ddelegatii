/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.ddelegatii.entity.Settings;

/**
 *
 * @author deimios
 */
@Local
public interface SettingsFacadeLocal {

    void create(Settings settings);

    void edit(Settings settings);

    void remove(Settings settings);

    Settings find(Object id);

    List<Settings> findAll();

    List<Settings> findRange(int[] range);

    int count();

    public java.lang.String getSetting(java.lang.String settingName);
    
}
