/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.ddelegatii.entity.Settings;

/**
 *
 * @author deimios
 */
@Stateless
public class SettingsFacade extends AbstractFacade<Settings> implements SettingsFacadeLocal {

    @PersistenceContext(unitName = "DDelegatiiPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SettingsFacade() {
        super(Settings.class);
    }

    @Override
    public String getSetting(String settingName) {
        Query query = em.createNamedQuery("Settings.findByKey");
        query.setParameter("key", settingName);
        return ((Settings) query.getSingleResult()).getValue();
    }
}
