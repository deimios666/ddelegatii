/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.ddelegatii.entity.User;

/**
 *
 * @author deimios
 */
@Stateless
public class UserFacade extends AbstractFacade<User> implements UserFacadeLocal {

    @PersistenceContext(unitName = "DDelegatiiPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserFacade() {
        super(User.class);
    }

    @Override
    public User findByUsername(String username) {
        Query query = em.createNamedQuery("User.findByUsername");
        query.setParameter("username", username);
        List<User> results = query.getResultList();
        if (results != null && results.size() == 1) {
            return results.get(0);
        } else {
            return null;
        }
    }
}
