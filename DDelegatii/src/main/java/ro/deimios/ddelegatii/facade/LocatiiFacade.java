/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.ddelegatii.entity.Locatii;

/**
 *
 * @author deimios
 */
@Stateless
public class LocatiiFacade extends AbstractFacade<Locatii> implements LocatiiFacadeLocal {

    @PersistenceContext(unitName = "DDelegatiiPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LocatiiFacade() {
        super(Locatii.class);
    }

    @Override
    public Locatii findByNume(String nume) {
        Query query = em.createNamedQuery("Locatii.findByNume");
        query.setParameter("nume", nume);
        List<Locatii> result = query.getResultList();
        try {
            Locatii dbLocatii = result.get(0);
            return dbLocatii;
        } catch (Exception e) {
            return null;
        }
    }
}
