package ro.deimios.ddelegatii.converter;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ro.deimios.ddelegatii.entity.Grupa;
import ro.deimios.ddelegatii.facade.GrupaFacadeLocal;

@FacesConverter(value = "grupaConverter")
public class GrupaConverter implements Converter {

    GrupaFacadeLocal grupaFacade = lookupGrupaFacadeLocal();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        String[] parts = value.split("\\|");
        return grupaFacade.find(Integer.valueOf(parts[0]));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        Grupa grupa = (Grupa) value;
        return "" + grupa.getId() + "|" + grupa.getGrupa() + "|" + grupa.getSubgrupa();
    }

    private GrupaFacadeLocal lookupGrupaFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (GrupaFacadeLocal) c.lookup("java:global/DDelegatii/GrupaFacade!ro.deimios.ddelegatii.facade.GrupaFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}