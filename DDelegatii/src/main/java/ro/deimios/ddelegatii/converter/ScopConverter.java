package ro.deimios.ddelegatii.converter;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ro.deimios.ddelegatii.entity.Scop;
import ro.deimios.ddelegatii.facade.ScopFacadeLocal;

/**
 *
 * @author deimios
 */
@FacesConverter(value = "scopConverter")
public class ScopConverter implements Converter {

    ScopFacadeLocal scopFacade = lookupScopFacadeLocal();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        String[] parts = value.split("\\|");
        return scopFacade.find(Integer.valueOf(parts[0]));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        Scop scop = (Scop) value;
        return "" + scop.getId() + "|" + scop.getLine1();
    }

    private ScopFacadeLocal lookupScopFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (ScopFacadeLocal) c.lookup("java:global/DDelegatii/ScopFacade!ro.deimios.ddelegatii.facade.ScopFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
