package ro.deimios.ddelegatii.converter;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ro.deimios.ddelegatii.entity.Locatii;
import ro.deimios.ddelegatii.facade.LocatiiFacadeLocal;

/**
 *
 * @author deimios
 */
@FacesConverter(value = "locatiiConverter")
public class LocatiiConverter implements Converter {

    LocatiiFacadeLocal locatiiFacade = lookupLocatiiFacadeLocal();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        String[] parts = value.split("\\|");
        return locatiiFacade.find(Integer.valueOf(parts[0]));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        Locatii locatie = (Locatii) value;
        return "" + locatie.getId() + "|" + locatie.getNume();
    }

    private LocatiiFacadeLocal lookupLocatiiFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (LocatiiFacadeLocal) c.lookup("java:global/DDelegatii/LocatiiFacade!ro.deimios.ddelegatii.facade.LocatiiFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
