package ro.deimios.ddelegatii.converter;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ro.deimios.ddelegatii.entity.Persoana;
import ro.deimios.ddelegatii.facade.PersoanaFacadeLocal;

/**
 *
 * @author deimios
 */
@FacesConverter(value = "persoanaConverter")
public class PersoanaConverter implements Converter {

    PersoanaFacadeLocal persoanaFacade = lookupPersoanaFacadeLocal();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        String[] parts = value.split("\\|");
        return persoanaFacade.find(Integer.valueOf(parts[0]));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        Persoana persoana = (Persoana) value;
        return "" + persoana.getId() + "|" + persoana.getNume();
    }

    private PersoanaFacadeLocal lookupPersoanaFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (PersoanaFacadeLocal) c.lookup("java:global/DDelegatii/PersoanaFacade!ro.deimios.ddelegatii.facade.PersoanaFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
