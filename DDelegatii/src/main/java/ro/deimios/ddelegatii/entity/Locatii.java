/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "locatii")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Locatii.findAll", query = "SELECT l FROM Locatii l"),
    @NamedQuery(name = "Locatii.findById", query = "SELECT l FROM Locatii l WHERE l.id = :id"),
    @NamedQuery(name = "Locatii.findByNume", query = "SELECT l FROM Locatii l WHERE l.nume = :nume")})
public class Locatii implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 254)
    @Column(name = "nume")
    private String nume;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "locatiiId")
    private List<Delegatii> delegatiiList;

    public Locatii() {
    }

    public Locatii(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    @XmlTransient
    public List<Delegatii> getDelegatiiList() {
        return delegatiiList;
    }

    public void setDelegatiiList(List<Delegatii> delegatiiList) {
        this.delegatiiList = delegatiiList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Locatii)) {
            return false;
        }
        Locatii other = (Locatii) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.ddelegatii.entity.Locatii[ id=" + id + " ]";
    }
    
}
