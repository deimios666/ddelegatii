/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "delegatii")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Delegatii.findAll", query = "SELECT d FROM Delegatii d"),
    @NamedQuery(name = "Delegatii.getMaxNr", query = "SELECT max(d.nr) FROM Delegatii d"),
    @NamedQuery(name = "Delegatii.findById", query = "SELECT d FROM Delegatii d WHERE d.id = :id"),
    @NamedQuery(name = "Delegatii.findByNr", query = "SELECT d FROM Delegatii d WHERE d.nr = :nr"),
    @NamedQuery(name = "Delegatii.findByData", query = "SELECT d FROM Delegatii d WHERE d.data = :data"),
    @NamedQuery(name = "Delegatii.findByStartDate", query = "SELECT d FROM Delegatii d WHERE d.startDate = :startDate"),
    @NamedQuery(name = "Delegatii.deleteByMinMaxNr", query = "DELETE FROM Delegatii d WHERE d.nr >= :minNr AND d.nr <= :maxNr"),
    @NamedQuery(name = "Delegatii.findByEndDate", query = "SELECT d FROM Delegatii d WHERE d.endDate = :endDate")})
public class Delegatii implements Serializable {

    @JoinColumn(name = "scop_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Scop scopId;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nr")
    private Integer nr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "data")
    @Temporal(TemporalType.DATE)
    private Date data;
    @Basic(optional = false)
    @NotNull
    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @JoinColumn(name = "persoana_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Persoana persoanaId;
    @JoinColumn(name = "locatii_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Locatii locatiiId;

    public Delegatii() {
    }

    public Delegatii(Integer id) {
        this.id = id;
    }

    public Delegatii(Integer id, Integer nr, Date data, Date startDate) {
        this.id = id;
        this.nr = nr;
        this.data = data;
        this.startDate = startDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNr() {
        return nr;
    }

    public void setNr(Integer nr) {
        this.nr = nr;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Persoana getPersoanaId() {
        return persoanaId;
    }

    public void setPersoanaId(Persoana persoanaId) {
        this.persoanaId = persoanaId;
    }

    public Locatii getLocatiiId() {
        return locatiiId;
    }

    public void setLocatiiId(Locatii locatiiId) {
        this.locatiiId = locatiiId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Delegatii)) {
            return false;
        }
        Delegatii other = (Delegatii) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.ddelegatii.entity.Delegatii[ id=" + id + " ]";
    }

    public Scop getScopId() {
        return scopId;
    }

    public void setScopId(Scop scopId) {
        this.scopId = scopId;
    }
}
