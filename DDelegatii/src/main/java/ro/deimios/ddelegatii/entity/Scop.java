/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "scop")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Scop.findAll", query = "SELECT s FROM Scop s"),
    @NamedQuery(name = "Scop.findById", query = "SELECT s FROM Scop s WHERE s.id = :id"),
    @NamedQuery(name = "Scop.findByLine1", query = "SELECT s FROM Scop s WHERE s.line1 = :line1"),
    @NamedQuery(name = "Scop.findByLines", query = "SELECT s FROM Scop s WHERE s.line1 = :line1 AND s.line2 = :line2 AND s.line3 = :line3 AND s.line4 = :line4"),
    @NamedQuery(name = "Scop.findByLine2", query = "SELECT s FROM Scop s WHERE s.line2 = :line2"),
    @NamedQuery(name = "Scop.findByLine3", query = "SELECT s FROM Scop s WHERE s.line3 = :line3"),
    @NamedQuery(name = "Scop.findByLine4", query = "SELECT s FROM Scop s WHERE s.line4 = :line4")})
public class Scop implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Size(max = 250)
    @Column(name = "line1")
    private String line1;
    @Size(max = 250)
    @Column(name = "line2")
    private String line2;
    @Size(max = 250)
    @Column(name = "line3")
    private String line3;
    @Size(max = 250)
    @Column(name = "line4")
    private String line4;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "scopId")
    private List<Delegatii> delegatiiList;

    public Scop() {
    }

    public Scop(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getLine3() {
        return line3;
    }

    public void setLine3(String line3) {
        this.line3 = line3;
    }

    public String getLine4() {
        return line4;
    }

    public void setLine4(String line4) {
        this.line4 = line4;
    }

    @XmlTransient
    public List<Delegatii> getDelegatiiList() {
        return delegatiiList;
    }

    public void setDelegatiiList(List<Delegatii> delegatiiList) {
        this.delegatiiList = delegatiiList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Scop)) {
            return false;
        }
        Scop other = (Scop) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.ddelegatii.entity.Scop[ id=" + id + " ]";
    }
    
}
