/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "persoana")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Persoana.findAll", query = "SELECT p FROM Persoana p"),
    @NamedQuery(name = "Persoana.removeAll", query = "DELETE FROM Persoana p"),
    @NamedQuery(name = "Persoana.findById", query = "SELECT p FROM Persoana p WHERE p.id = :id"),
    @NamedQuery(name = "Persoana.findByNume", query = "SELECT p FROM Persoana p WHERE p.nume = :nume"),
    @NamedQuery(name = "Persoana.findByNumeLikeUpper", query = "SELECT p FROM Persoana p WHERE UPPER(p.nume) LIKE :nume ORDER BY UPPER(p.nume)"),
    @NamedQuery(name = "Persoana.findByGrupaSubgrupa", query = "SELECT p FROM Persoana p WHERE p.grupaId.grupa = :grupa AND p.grupaId.subgrupa = :subgrupa ORDER BY p.nume"),
    @NamedQuery(name = "Persoana.findByBuletin", query = "SELECT p FROM Persoana p WHERE p.buletin = :buletin")})
public class Persoana implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "nume")
    private String nume;
    @Size(max = 45)
    @Column(name = "buletin")
    private String buletin;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "persoanaId")
    private List<Delegatii> delegatiiList;
    @JoinColumn(name = "functia_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Functia functiaId;
    @JoinColumn(name = "grupa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Grupa grupaId;

    public Persoana() {
    }

    public Persoana(Integer id) {
        this.id = id;
    }

    public Persoana(Integer id, String nume) {
        this.id = id;
        this.nume = nume;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getBuletin() {
        return buletin;
    }

    public void setBuletin(String buletin) {
        this.buletin = buletin;
    }

    @XmlTransient
    public List<Delegatii> getDelegatiiList() {
        return delegatiiList;
    }

    public void setDelegatiiList(List<Delegatii> delegatiiList) {
        this.delegatiiList = delegatiiList;
    }

    public Functia getFunctiaId() {
        return functiaId;
    }

    public void setFunctiaId(Functia functiaId) {
        this.functiaId = functiaId;
    }

    public Grupa getGrupaId() {
        return grupaId;
    }

    public void setGrupaId(Grupa grupaId) {
        this.grupaId = grupaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persoana)) {
            return false;
        }
        Persoana other = (Persoana) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.ddelegatii.entity.Persoana[ id=" + id + " ]";
    }
}
