/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.ddelegatii.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "grupa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Grupa.findAll", query = "SELECT g FROM Grupa g"),
    @NamedQuery(name = "Grupa.findById", query = "SELECT g FROM Grupa g WHERE g.id = :id"),
    @NamedQuery(name = "Grupa.findByGrupa", query = "SELECT g FROM Grupa g WHERE g.grupa = :grupa"),
    @NamedQuery(name = "Grupa.findByGrupaSubgrupa", query = "SELECT g FROM Grupa g WHERE g.grupa = :grupa AND g.subgrupa = :subgrupa"),
    @NamedQuery(name = "Grupa.findBySubgrupa", query = "SELECT g FROM Grupa g WHERE g.subgrupa = :subgrupa")})
public class Grupa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "grupa")
    private String grupa;
    @Size(max = 45)
    @Column(name = "subgrupa")
    private String subgrupa;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "grupaId")
    private List<Persoana> persoanaList;

    public Grupa() {
    }

    public Grupa(Integer id) {
        this.id = id;
    }

    public Grupa(Integer id, String grupa) {
        this.id = id;
        this.grupa = grupa;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGrupa() {
        return grupa;
    }

    public void setGrupa(String grupa) {
        this.grupa = grupa;
    }

    public String getSubgrupa() {
        return subgrupa;
    }

    public void setSubgrupa(String subgrupa) {
        this.subgrupa = subgrupa;
    }

    @XmlTransient
    public List<Persoana> getPersoanaList() {
        return persoanaList;
    }

    public void setPersoanaList(List<Persoana> persoanaList) {
        this.persoanaList = persoanaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grupa)) {
            return false;
        }
        Grupa other = (Grupa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.ddelegatii.entity.Grupa[ id=" + id + " ]";
    }
    
}
